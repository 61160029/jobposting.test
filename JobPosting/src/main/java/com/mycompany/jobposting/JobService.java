/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jobposting;

import java.time.LocalDate;

/**
 *
 * @author user
 */
public class JobService {
    public static boolean checkEnableTime(LocalDate startTime, LocalDate endTime, LocalDate today) {
        if(today.isBefore(startTime)){
            return false;
        }
        else if(today.isAfter(endTime)){
            return false;
        }
        return true;
    }
    
}
