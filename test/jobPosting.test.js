const assert = require('assert')
const { describe } = require('mocha')
const { checkEnableTime } = require("../JobPosting")
describe('JobPosting', function () {
  it('Case:1 should return true when เวลาสมัครอยู่ระหว่างเริ่มต้นและสิ้นสุด', function () {
    // Arrage
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 3)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case:2 should return true when เวลาสมัครเท่ากับเวลาเริ่มต้น', function () {
    // Arrage
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 31)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case:3 should return true when เวลาสมัครเท่ากับเวลาสิ้นสุด', function () {
    // Arrage
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 5)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case:4 should return false when เวลาสมัครอยู่ก่อนเวลาเริ่มต้น', function () {
    // Arrage
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 30)
    const expectedResult = false
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case:5 should return false when เวลาสมัครอยู่หลังเวลาสิ้นสุด', function () {
    // Arrage
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 6)
    const expectedResult = false
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
})
/* if(actualResult === expectedResult) {
  console.log('เวลาอยู่ระหว่าง เริ่มต้นและสิ้นสุด PASSED')
}else{
  console.log('เวลาอยู่ระหว่าง เริ่มต้นและสิ้นสุด FAIL')
}
}) */